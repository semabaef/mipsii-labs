from sklearn.svm import SVC


class Classifier:
    def __init__(self):
        self.model = SVC(kernel="linear", C=1.0)

    def train(self, x, y):
        self.model.fit(x, y)

    def predict(self, x):
        return self.model.predict(x)
